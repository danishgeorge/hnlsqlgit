﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using HNL.Configuration.Dto;

namespace HNL.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : HNLAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
