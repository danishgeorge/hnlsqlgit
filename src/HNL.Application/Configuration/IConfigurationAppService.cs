﻿using System.Threading.Tasks;
using HNL.Configuration.Dto;

namespace HNL.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
