﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Entities.City.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.City
{
    [AbpAuthorize(PermissionNames.Pages_City)]
    public class CityAppService : AsyncCrudAppService<CityInfo, CityDto, long, PagedResultRequestDto, CityDto, CityDto>, ICityAppService
    {
        private readonly IRepository<CityInfo, long> _cityRepository;
        private readonly IPermissionManager _permissionManager;

        public CityAppService(IRepository<CityInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _cityRepository = _repository;
            _permissionManager = _Manager;
        }

        [AbpAuthorize(PermissionNames.Pages_City_Create)]
        public override async Task<CityDto> CreateAsync(CityDto input)
        {
            var result = ObjectMapper.Map<CityInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.TenantId);
            result.IsActive = true;
            await _cityRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<CityDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_City_Update)]
        public override async Task<CityDto> UpdateAsync(CityDto input)
        {
            var data = ObjectMapper.Map<CityInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _cityRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CityDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_City_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _cityRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _cityRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_City_Read)]
        [HttpPost]
        public List<CityDto> GetAllCities()
        {
            var data = _cityRepository.GetAll().Where(x => x.IsActive == true);
            return new List<CityDto>(ObjectMapper.Map<List<CityDto>>(data));
        }

       
            
    }
}
