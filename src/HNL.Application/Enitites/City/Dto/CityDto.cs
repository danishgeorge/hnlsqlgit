﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HNL.Enitites;
using System;
using System.Collections.Generic;
using System.Text;

namespace HNL.Entities.City.Dto
{
    [AutoMapTo(typeof(CityInfo)), AutoMapFrom(typeof(CityInfo))]
    public class CityDto:EntityDto<long>
    {
        public string Name { get; set; }
    }
}
