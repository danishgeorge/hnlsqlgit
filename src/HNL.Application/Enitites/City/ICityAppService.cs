﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Entities.City.Dto;
using System.Collections.Generic;

namespace HNL.Entities.City
{
    public interface ICityAppService : IAsyncCrudAppService< CityDto, long, PagedResultRequestDto, CityDto, CityDto>
    {
        List<CityDto> GetAllCities();
        //Task<CityEditDto> GetRoleForEdit(EntityDto input);
    }
}
