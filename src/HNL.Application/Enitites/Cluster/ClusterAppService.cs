﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Cluster.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Cluster
{   
    [AbpAuthorize(PermissionNames.Pages_Cluster)]
    public class ClusterAppService : AsyncCrudAppService<ClusterInfo, ClusterDto, long, PagedResultRequestDto, ClusterDto, ClusterDto>, IClusterAppService
    {
        private readonly IRepository<ClusterInfo, long> _Clusterrepository;
        private readonly IPermissionManager _permissionManager;

        public ClusterAppService(IRepository<ClusterInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _Clusterrepository = _repository;
            _permissionManager = _Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_Cluster_Create)]
        public override async Task<ClusterDto> CreateAsync(ClusterDto input)
        {
            var result = ObjectMapper.Map<ClusterInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _Clusterrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<ClusterDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_Cluster_Update)]
        public override async Task<ClusterDto> UpdateAsync(ClusterDto input)
        {
            var data = ObjectMapper.Map<ClusterInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _Clusterrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ClusterDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Cluster_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _Clusterrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _Clusterrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Cluster_Read)]
        [HttpPost]
        public List<ClusterDto> GetAllCluster()
        {
            var data = _Clusterrepository.GetAll().Where(x => x.IsActive == true);
            return new List<ClusterDto>(ObjectMapper.Map<List<ClusterDto>>(data));
        }

       
            
    }
}
