﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Cluster.Dto
{
    [AutoMapTo(typeof(ClusterInfo)), AutoMapFrom(typeof(ClusterInfo))]

    public class ClusterDto : EntityDto<long>
    {
        public string Name { get; set; }

    }
   
}
