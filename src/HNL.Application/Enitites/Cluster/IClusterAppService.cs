﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Cluster.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Cluster
{
    public interface IClusterAppService : IAsyncCrudAppService< ClusterDto, long, PagedResultRequestDto, ClusterDto, ClusterDto>
    {
        List<ClusterDto> GetAllCluster();
        //Task<ClusterEditDto> GetRoleForEdit(EntityDto input);
    }
}
