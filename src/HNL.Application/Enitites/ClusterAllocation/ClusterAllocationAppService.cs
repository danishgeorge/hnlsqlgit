﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.ClusterAllocation.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.ClusterAllocation
{
    [AbpAuthorize(PermissionNames.Pages_ClusterAllocation)]
    public class ClusterAllocationAppService : AsyncCrudAppService<ClusterAllocationInfo, ClusterAllocationDto, long, PagedResultRequestDto, ClusterAllocationDto, ClusterAllocationDto>, IClusterAllocationAppService
    {
        private readonly IRepository<ClusterAllocationInfo, long> _ClusterAllocationRepository;
        private readonly IPermissionManager _permissionManager;

        public ClusterAllocationAppService(IRepository<ClusterAllocationInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ClusterAllocationRepository = _repository;
            _permissionManager = _Manager;
        }
        
        [AbpAuthorize(PermissionNames.Pages_ClusterAllocation_Create)]
        public override async Task<ClusterAllocationDto> CreateAsync(ClusterAllocationDto input)
        {
            var result = ObjectMapper.Map<ClusterAllocationInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _ClusterAllocationRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<ClusterAllocationDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_ClusterAllocation_Update)]
        public override async Task<ClusterAllocationDto> UpdateAsync(ClusterAllocationDto input)
        {
            var data = ObjectMapper.Map<ClusterAllocationInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _ClusterAllocationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ClusterAllocationDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_ClusterAllocation_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _ClusterAllocationRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _ClusterAllocationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_ClusterAllocation_Read)]
        [HttpPost]
        public List<ClusterAllocationDto> GetAllClusterAllocation()
        {
            var data = _ClusterAllocationRepository.GetAll().Where(x => x.IsActive == true);
            return new List<ClusterAllocationDto>(ObjectMapper.Map<List<ClusterAllocationDto>>(data));
        }

       
            
    }
}
