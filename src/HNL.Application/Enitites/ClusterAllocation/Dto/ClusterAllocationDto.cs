﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.ClusterAllocation.Dto
{
    [AutoMapTo(typeof(ClusterAllocationInfo)), AutoMapFrom(typeof(ClusterAllocationInfo))]

    public class ClusterAllocationDto : EntityDto<long>
    {
        public string ErpId { get; set; }
        public long ClusterId { get; set; }

    }
   
}
