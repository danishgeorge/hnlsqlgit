﻿    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using HNL.Enitites.ClusterAllocation.Dto;
    using System.Collections.Generic;

    namespace HNL.Entities.ClusterAllocation
    {
        public interface IClusterAllocationAppService : IAsyncCrudAppService< ClusterAllocationDto, long, PagedResultRequestDto, ClusterAllocationDto, ClusterAllocationDto>
        {
            List<ClusterAllocationDto> GetAllClusterAllocation();
            //Task<ClusterAllocationEditDto> GetRoleForEdit(EntityDto input);
        }
    }
