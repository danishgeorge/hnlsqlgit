﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Country.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Country
{
    [AbpAuthorize(PermissionNames.Pages_Country)]
    public class CountryAppService : AsyncCrudAppService<CountryInfo, CountryDto, long, PagedResultRequestDto, CountryDto, CountryDto>, ICountryAppService
    {
        private readonly IRepository<CountryInfo, long> _Countryrepository;
        private readonly IPermissionManager _permissionManager;

        public CountryAppService(IRepository<CountryInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _Countryrepository = _repository;
            _permissionManager = _Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_Country_Create)]
        public override async Task<CountryDto> CreateAsync(CountryDto input)
        {
            var result = ObjectMapper.Map<CountryInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _Countryrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<CountryDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_Country_Update)]
        public override async Task<CountryDto> UpdateAsync(CountryDto input)
        {
            var data = ObjectMapper.Map<CountryInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _Countryrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<CountryDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Country_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _Countryrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _Countryrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Country_Read)]
        [HttpPost]
        public List<CountryDto> GetAllCountry()
        {
            var data = _Countryrepository.GetAll().Where(x => x.IsActive == true);
            return new List<CountryDto>(ObjectMapper.Map<List<CountryDto>>(data));
        }

       
            
    }
}
