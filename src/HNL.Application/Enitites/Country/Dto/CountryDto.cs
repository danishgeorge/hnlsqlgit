﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Country.Dto
{
    [AutoMapTo(typeof(CountryInfo)), AutoMapFrom(typeof(CountryInfo))]

    public class CountryDto : EntityDto<long>
    {
        public string Name { get; set; }

    }
   
}
