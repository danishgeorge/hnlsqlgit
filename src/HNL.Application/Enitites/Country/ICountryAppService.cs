﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Country.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Country
{
    public interface ICountryAppService : IAsyncCrudAppService< CountryDto, long, PagedResultRequestDto, CountryDto, CountryDto>
    {
        List<CountryDto> GetAllCountry();
        //Task<CountryEditDto> GetRoleForEdit(EntityDto input);
    }
}
