﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Department.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Department
{
    [AbpAuthorize(PermissionNames.Pages_Department)]
    public class DepartmentAppService : AsyncCrudAppService<DepartmentInfo, DepartmentDto, long, PagedResultRequestDto, DepartmentDto, DepartmentDto>, IDepartmentAppService
    {
        private readonly IRepository<DepartmentInfo, long> _Departmentrepository;
        private readonly IPermissionManager _permissionManager;

        public DepartmentAppService(IRepository<DepartmentInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _Departmentrepository = _repository;
            _permissionManager = _Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_Department_Create)]
        public override async Task<DepartmentDto> CreateAsync(DepartmentDto input)
        {
            var result = ObjectMapper.Map<DepartmentInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _Departmentrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<DepartmentDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_Department_Update)]
        public override async Task<DepartmentDto> UpdateAsync(DepartmentDto input)
        {
            var data = ObjectMapper.Map<DepartmentInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _Departmentrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<DepartmentDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Department_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _Departmentrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _Departmentrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Department_Read)]
        [HttpPost]
        public List<DepartmentDto> GetAllDepartment()
        {
            var data = _Departmentrepository.GetAll().Where(x => x.IsActive == true);
            return new List<DepartmentDto>(ObjectMapper.Map<List<DepartmentDto>>(data));
        }

       
            
    }
}
