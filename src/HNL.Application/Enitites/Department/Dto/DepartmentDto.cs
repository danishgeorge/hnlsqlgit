﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Department.Dto
{
    [AutoMapTo(typeof(DepartmentInfo)), AutoMapFrom(typeof(DepartmentInfo))]

    public class DepartmentDto : EntityDto<long>
    {
        public string Name { get; set; }

    }
   
}
