﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Department.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Department
{
    public interface IDepartmentAppService : IAsyncCrudAppService< DepartmentDto, long, PagedResultRequestDto, DepartmentDto, DepartmentDto>
    {
        List<DepartmentDto> GetAllDepartment();
        //Task<DepartmentEditDto> GetRoleForEdit(EntityDto input);
    }
}
