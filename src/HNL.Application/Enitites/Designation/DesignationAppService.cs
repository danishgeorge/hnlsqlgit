﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Designation.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Designation
{
    [AbpAuthorize(PermissionNames.Pages_Designation)]
    public class DesignationAppService : AsyncCrudAppService<DesignationInfo, DesignationDto, long, PagedResultRequestDto, DesignationDto, DesignationDto>, IDesignationAppService
    {
        private readonly IRepository<DesignationInfo, long> _Designationrepository;
        private readonly IPermissionManager _permissionManager;

        public DesignationAppService(IRepository<DesignationInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _Designationrepository = _repository;
            _permissionManager = _Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_Designation_Create)]
        public override async Task<DesignationDto> CreateAsync(DesignationDto input)
        {
            var result = ObjectMapper.Map<DesignationInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _Designationrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<DesignationDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_Designation_Update)]
        public override async Task<DesignationDto> UpdateAsync(DesignationDto input)
        {
            var data = ObjectMapper.Map<DesignationInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _Designationrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<DesignationDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Designation_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _Designationrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _Designationrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Designation_Read)]
        [HttpPost]
        public List<DesignationDto> GetAllDesignation()
        {
            var data = _Designationrepository.GetAll().Where(x => x.IsActive == true);
            return new List<DesignationDto>(ObjectMapper.Map<List<DesignationDto>>(data));
        }

       
            
    }
}
