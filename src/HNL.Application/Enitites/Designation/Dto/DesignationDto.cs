﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Designation.Dto
{
    [AutoMapTo(typeof(DesignationInfo)), AutoMapFrom(typeof(DesignationInfo))]

    public class DesignationDto : EntityDto<long>
    {
        public string Name { get; set; }

    }
   
}
