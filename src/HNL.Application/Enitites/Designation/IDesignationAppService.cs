﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Designation.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Designation
{
    public interface IDesignationAppService : IAsyncCrudAppService< DesignationDto, long, PagedResultRequestDto, DesignationDto, DesignationDto>
    {
        List<DesignationDto> GetAllDesignation();
        //Task<DesignationEditDto> GetRoleForEdit(EntityDto input);
    }
}
