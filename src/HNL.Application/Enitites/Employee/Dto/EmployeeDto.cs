﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HNL.Enitites;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace HNL.Entities.Employee.Dto
{
    [AutoMapTo(typeof(EmployeeInfo)), AutoMapFrom(typeof(EmployeeInfo))]
    public class EmployeeDto : EntityDto<long>
    {
        public string Designation { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Project { get; set; }
        public string Department { get; set; }
        public string Region { get; set; }
        public string Cluster { get; set; }
        public long UserId { get; set; }
        public string EmailAddress { get; set; }
        public string ErpId { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string CNIC { get; set; }
        public string SiteAllocation { get; set; }
        public string CellNo { get; set; }
        public string Loc { get; set; }
        //public string Password { get; set; }
        public string DeviceToken { get; set; }
        public string ProfileImage { get; set; }

    }
}