﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.UI;
using HNL.Authorization;
using HNL.Authorization.Roles;
using HNL.Authorization.Users;
using HNL.Enitites;
using HNL.Enitites.Cluster.Dto;
using HNL.Enitites.ClusterAllocation.Dto;
using HNL.Enitites.Country.Dto;
using HNL.Enitites.Department.Dto;
using HNL.Enitites.Designation.Dto;
using HNL.Enitites.Employee;
using HNL.Enitites.Project.Dto;
using HNL.Enitites.ProjectAllocation.Dto;
using HNL.Enitites.Region.Dto;
using HNL.Enitites.RegionAllocation.Dto;
using HNL.Enitites.SiteAllocation.Dto;
using HNL.Entities.City.Dto;
using HNL.Entities.Employee.Dto;
using HNL.Users.Dto;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Employee
{
    [AbpAuthorize(PermissionNames.Pages_Employee)]
    public class EmployeeAppService : AsyncCrudAppService<EmployeeInfo, EmployeeDto, long, PagedResultRequestDto, EmployeeDto, EmployeeDto>, IEmployeeAppService
    {
        private readonly IRepository<EmployeeInfo, long> _employeeRepository;
        private readonly IPermissionManager _permissionManager;


        private readonly IRepository<DepartmentInfo, long> _departmentRepository;
        private readonly IRepository<DesignationInfo, long> _designationRepository;
        private readonly IRepository<ProjectInfo, long> _projectRepository;
        private readonly IRepository<RegionInfo, long> _regionRepository;
        private readonly IRepository<ClusterInfo, long> _clusterRepository;
        private readonly IRepository<CityInfo, long> _cityRepository;
        private readonly IRepository<CountryInfo, long> _countryRepository;
        private readonly IRepository<SiteInfo, long> _siteRepository;
        private readonly IRepository<User, long> _userRepository;


        private readonly IRepository<SiteAllocationInfo, long> _siteAllocationRepository;
        private readonly IRepository<ProjectAllocationInfo, long> _projectAllocationRepository;
        private readonly IRepository<RegionAllocationInfo, long> _regionAllocationRepository;
        private readonly IRepository<ClusterAllocationInfo, long> _clusterAllocationRepository;

        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;


        public EmployeeAppService(
            IRepository<EmployeeInfo, long> employeeRepository, 
           
            IRepository<DepartmentInfo, long> departmentRepository,
            IRepository<DesignationInfo, long> designationRepository,
            IRepository<ProjectInfo, long> projectRepository, 
            IRepository<RegionInfo, long> regionrepository, 
            IRepository<ClusterInfo, long> clusterRepository, 
            IRepository<CityInfo, long> cityRepository,
            IRepository<CountryInfo, long> countryRepository,
            IRepository<SiteInfo, long> siteRepository,
            IRepository<SiteAllocationInfo, long> siteAllocationRepository,
            IRepository<ProjectAllocationInfo, long> projectAllocationRepository,
            IRepository<RegionAllocationInfo, long> regionAllocationRepository,
            IRepository<ClusterAllocationInfo, long> clusterAllocationRepository,

            IRepository<User, long> userRepository,
            UserManager userManager,
            RoleManager roleManager,

            IPermissionManager _Manager)  : base(employeeRepository)
        {
            _departmentRepository = departmentRepository;
            _designationRepository = designationRepository;
            _projectRepository = projectRepository;
            _clusterRepository = clusterRepository;
            _employeeRepository = employeeRepository;
            _permissionManager = _Manager;
            _regionRepository = regionrepository;
            _cityRepository = cityRepository;
            _siteRepository = siteRepository;
            _countryRepository = countryRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _roleManager = roleManager;


            _siteAllocationRepository = siteAllocationRepository;
            _projectAllocationRepository = projectAllocationRepository;
            _regionAllocationRepository = regionAllocationRepository;
            _clusterAllocationRepository = clusterAllocationRepository;
    }
        [AbpAuthorize(PermissionNames.Pages_Employee_Create)]
        public override async Task<EmployeeDto> CreateAsync(EmployeeDto input)
        {
            var dataEmployee = _employeeRepository.GetAll().Where(x => x.ErpId == input.ErpId && x.TenantId == AbpSession.TenantId).FirstOrDefault();
            if (dataEmployee != null)
            {
                throw new UserFriendlyException("there is already site entered with that name");
            }

            var dataUserToCheckDuplicate = _userRepository.GetAll().Where(x => x.UserName == input.ErpId && x.TenantId == AbpSession.TenantId).FirstOrDefault();

            if (dataUserToCheckDuplicate != null)
            {
                throw new UserFriendlyException("there is already user entered with that name");
            }
            ///

            CheckCreatePermission();

            CreateUserDto userDto = new CreateUserDto();
            userDto.UserName = input.ErpId;
            userDto.Name = input.Name;
            userDto.Surname = input.FatherName;
            string[] RoleNames = { "HNLEmployee" };
            userDto.RoleNames = RoleNames;
            userDto.Password = "hnl123";
            userDto.CountryId = "1";
            userDto.EmailAddress = input.ErpId+"@hnl.com.pk";
            userDto.IsActive = true;

            var user = ObjectMapper.Map<User>(userDto);

            user.TenantId = AbpSession.TenantId;
            user.IsEmailConfirmed = true;
            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);

            CheckErrors(await _userManager.CreateAsync(user, userDto.Password));

            if (userDto.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRolesAsync(user, userDto.RoleNames));
            }



            var dataUser = user;



            EmployeeDto emp = new EmployeeDto();

            try
            {
                //--------------> Region
                List<string> regions = new List<string>(input.Region.Split(','));
                regions.RemoveAll(x => x == "");
               
                foreach( var region in regions)
                {
                    var dataRegion = _regionRepository.GetAll().Where(x => x.Name == region && x.TenantId == AbpSession.TenantId).FirstOrDefault();

                    if (dataRegion == null)
                    {
                        RegionDto regionDto = new RegionDto();
                        regionDto.Name = region;
                        var resultR = ObjectMapper.Map<RegionInfo>(regionDto);
                        resultR.CreationTime = DateTime.Now;
                        resultR.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultR.TenantId = Convert.ToInt32(AbpSession.TenantId);
                        resultR.IsActive = true;
                        await _regionRepository.InsertAsync(resultR);
                        CurrentUnitOfWork.SaveChanges();
                        dataRegion = resultR;

                    }

                    RegionAllocationDto regionAllocationDto = new RegionAllocationDto();
                    regionAllocationDto.ErpId = input.ErpId;
                    regionAllocationDto.RegionId = dataRegion.Id;

                    var resultPA = ObjectMapper.Map<RegionAllocationInfo>(regionAllocationDto);
                    resultPA.CreationTime = DateTime.Now;
                    resultPA.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultPA.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultPA.IsActive = true;

                    await _regionAllocationRepository.InsertAsync(resultPA);
                    CurrentUnitOfWork.SaveChanges();


                }

                //--------------> Project
                List<string> projects = new List<string>(input.Project.Split(','));
                projects.RemoveAll(x => x == "");

                foreach (var project in projects)
                {
                    var dataProject = _projectRepository.GetAll().Where(x => x.Name == project && x.TenantId == AbpSession.TenantId).FirstOrDefault();

                    if (dataProject == null)
                    {
                        ProjectDto projectDto = new ProjectDto();
                        projectDto.Name = project;
                        var resultP = ObjectMapper.Map<ProjectInfo>(projectDto);
                        resultP.CreationTime = DateTime.Now;
                        resultP.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultP.TenantId = Convert.ToInt32(AbpSession.TenantId);
                        resultP.IsActive = true;
                        await _projectRepository.InsertAsync(resultP);
                          CurrentUnitOfWork.SaveChanges();
                        dataProject = resultP;

                    }

                    ProjectAllocationDto projectAllocationDto = new ProjectAllocationDto();
                    projectAllocationDto.ErpId = input.ErpId;
                    projectAllocationDto.ProjectId = dataProject.Id;

                    var resultPA = ObjectMapper.Map<ProjectAllocationInfo>(projectAllocationDto);
                    resultPA.CreationTime = DateTime.Now;
                    resultPA.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultPA.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultPA.IsActive = true;

                    await _projectAllocationRepository.InsertAsync(resultPA);
                    CurrentUnitOfWork.SaveChanges();


                }





                //-----------> Cluster

                List<string> clusters = new List<string>(input.Cluster.Split(','));
                clusters.RemoveAll(x => x == "");

                foreach (var cluster in clusters)
                {
                
                    var dataCluster = _clusterRepository.GetAll().Where(x => x.Name == cluster && x.TenantId == AbpSession.TenantId).FirstOrDefault();

                    if (dataCluster == null)
                    {
                        ClusterDto clusterDto = new ClusterDto();
                        clusterDto.Name = input.Cluster;
                        var resultR = ObjectMapper.Map<ClusterInfo>(clusterDto);
                        resultR.CreationTime = DateTime.Now;
                        resultR.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultR.TenantId = Convert.ToInt32(AbpSession.TenantId);
                        resultR.IsActive = true;
                        await _clusterRepository.InsertAsync(resultR);
                        CurrentUnitOfWork.SaveChanges();
                        dataCluster = resultR;

                    }

                    ClusterAllocationDto clusterAllocationDto = new ClusterAllocationDto();
                    clusterAllocationDto.ErpId = input.ErpId;
                    clusterAllocationDto.ClusterId = dataCluster.Id;

                    var resultCA = ObjectMapper.Map<ClusterAllocationInfo>(clusterAllocationDto);
                    resultCA.CreationTime = DateTime.Now;
                    resultCA.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultCA.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultCA.IsActive = true;

                    await _clusterAllocationRepository.InsertAsync(resultCA);
                    CurrentUnitOfWork.SaveChanges();

                }



                //------------> Site Allocation
                List<string> sites = new List<string>(input.SiteAllocation.Split(','));
                sites.RemoveAll(x => x == "");

                foreach (var site in sites)
                { 
                   
                    var dataSite = _siteRepository.GetAll().Where(x => x.SiteName == site && x.TenantId == AbpSession.TenantId).FirstOrDefault();


                    if (dataSite != null)
                    {
                        SiteAllocationDto siteAllocationDto = new SiteAllocationDto();
                        siteAllocationDto.ErpId = input.ErpId;
                        siteAllocationDto.SiteId = dataSite.Id;

                        var resultSA = ObjectMapper.Map<SiteAllocationInfo>(siteAllocationDto);
                        resultSA.CreationTime = DateTime.Now;
                        resultSA.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultSA.TenantId = Convert.ToInt32(AbpSession.TenantId);
                        resultSA.IsActive = true;
                        await _siteAllocationRepository.InsertAsync(resultSA);
                        CurrentUnitOfWork.SaveChanges();


                    }
                }

                //------------> CityInfo

                var dataCity = _cityRepository.GetAll().Where(x => x.Name == input.City && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                if (dataCity == null)
                {
                    CityDto cityDto = new CityDto();
                    cityDto.Name = input.City;
                    var resultCity = ObjectMapper.Map<CityInfo>(cityDto);
                    resultCity.CreationTime = DateTime.Now;
                    resultCity.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultCity.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultCity.IsActive = true;
                    await _cityRepository.InsertAsync(resultCity);
                    CurrentUnitOfWork.SaveChanges();
                    dataCity = resultCity;
                }


                //------------> CountryInfo

                var dataCountry = _countryRepository.GetAll().Where(x => x.Name == input.Country && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                if (dataCountry == null)
                {
                    CountryDto countryDto = new CountryDto();
                    countryDto.Name = input.Country;
                    var resultCo = ObjectMapper.Map<CountryInfo>(countryDto);
                    resultCo.CreationTime = DateTime.Now;
                    resultCo.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultCo.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultCo.IsActive = true;
                    await _countryRepository.InsertAsync(resultCo);
                    CurrentUnitOfWork.SaveChanges();
                    dataCountry = resultCo;
                }

                //------------> DesignationInfo

                var dataDesignation = _designationRepository.GetAll().Where(x => x.Name == input.Designation && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                if (dataDesignation == null)
                {
                    DesignationDto designationDto = new DesignationDto();
                    designationDto.Name = input.Designation;
                    var resultDes = ObjectMapper.Map<DesignationInfo>(designationDto);
                    resultDes.CreationTime = DateTime.Now;
                    resultDes.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultDes.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultDes.IsActive = true;
                    await _designationRepository.InsertAsync(resultDes);
                    CurrentUnitOfWork.SaveChanges();
                    dataDesignation = resultDes;
                }

                //------------> DepartementInfo

                var dataDepartment = _departmentRepository.GetAll().Where(x => x.Name == input.Department && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                if (dataDepartment == null)
                {
                    DepartmentDto departmentDto = new DepartmentDto();
                    departmentDto.Name = input.Department;
                    var resultD = ObjectMapper.Map<DepartmentInfo>(departmentDto);
                    resultD.CreationTime = DateTime.Now;
                    resultD.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultD.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultD.IsActive = true;
                    await _departmentRepository.InsertAsync(resultD);
                    CurrentUnitOfWork.SaveChanges();
                    dataDepartment = resultD;
                }

                
                        var result = ObjectMapper.Map<EmployeeInfo>(input);
                        
                        result.CityInfoId = dataCity.Id;
                        result.CountryInfoId = dataCountry.Id;
                        result.DepartmentInfoId = dataDepartment.Id;
                        result.DesignationInfoId = dataDesignation.Id;
                        result.CreationTime = DateTime.Now;
                        result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        result.TenantId = Convert.ToInt32(AbpSession.TenantId);
                        result.UserId = dataUser.Id;
                        result.IsActive = true;

                        await _employeeRepository.InsertAsync(result);
                        CurrentUnitOfWork.SaveChanges();
                        var data = result.MapTo<EmployeeDto>();
                        return data;

                    }
                    catch (Exception ex)
                    {

                    }
                

                
                    return emp;
            } 
        
        [AbpAuthorize(PermissionNames.Pages_Employee_Update)]
        public override async Task<EmployeeDto> UpdateAsync(EmployeeDto input)
        {
            var data = ObjectMapper.Map<EmployeeInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _employeeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<EmployeeDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Employee_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _employeeRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _employeeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
          [AbpAuthorize(PermissionNames.Pages_Employee_Update)]
        [HttpPost]
        public List<EmployeeDto> GetAllEmployee()
        {
            var data = _employeeRepository.GetAll().Where(x => x.IsActive == false);
            return new List<EmployeeDto>(ObjectMapper.Map<List<EmployeeDto>>(data));
        }


        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        //public List<EmployeeDto> GetEmployeebyNameAsync(string name)
        //{
        //    var data =  _Employeerepository.GetAll().Where(x => x.DesignationInfo.Name == Er  );

        //}

    }
}
