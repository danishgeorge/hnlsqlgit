﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Entities.Employee.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace HNL.Enitites.Employee
{
    public interface IEmployeeAppService : IAsyncCrudAppService<EmployeeDto, long, PagedResultRequestDto, EmployeeDto, EmployeeDto>
    {
        List<EmployeeDto> GetAllEmployee();
        //List<EmployeeDto> GetEmployeebyNameAsync(string name);
        //Task<EmployeeEditDto> GetRoleForEdit(EntityDto input);
    }
}
