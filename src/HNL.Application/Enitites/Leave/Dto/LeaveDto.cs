﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Leave.Dto
{
    [AutoMapTo(typeof(LeaveInfo)), AutoMapFrom(typeof(LeaveInfo))]

    public class LeaveDto : EntityDto<long>
    {
        public long LeaveTypeId { get; set; }
        public string ErpId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
}
   

