﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Leave.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Leave
{
    public interface ILeaveAppService : IAsyncCrudAppService< LeaveDto, long, PagedResultRequestDto, LeaveDto, LeaveDto>
    {
        List<LeaveDto> GetAllLeave();
        //Task<LeaveEditDto> GetRoleForEdit(EntityDto input);
    }
}
