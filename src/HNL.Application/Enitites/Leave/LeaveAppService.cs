﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Leave.Dto;
using HNL.Enitites.LeaveType.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Leave
{   
    [AbpAuthorize(PermissionNames.Pages_Leave)]
    public class LeaveAppService : AsyncCrudAppService<LeaveInfo, LeaveDto, long, PagedResultRequestDto, LeaveDto, LeaveDto>, ILeaveAppService
    {
        private readonly IRepository<LeaveInfo, long> _Leaverepository;
        private readonly IRepository<LeaveTypeInfo, long> _leaveTypeRepository;

        private readonly IPermissionManager _permissionManager;

        public LeaveAppService(IRepository<LeaveInfo, long> _repository, IRepository<LeaveTypeInfo, long> leaveTypeRepository,
            IPermissionManager _Manager) : base(_repository)
        {
            _Leaverepository = _repository;
            _leaveTypeRepository = leaveTypeRepository;
            _permissionManager = _Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_Leave_Create)]
        public override async Task<LeaveDto> CreateAsync(LeaveDto input)
        {

            var leaveData = _leaveTypeRepository.GetAll().Where(x => x.IsActive == true && x.Id== input.LeaveTypeId).FirstOrDefault();
            if (leaveData == null)
            {
                throw new UserFriendlyException("Leavetype is not correct");
            }

            var date = (input.EndDate - input.StartDate).TotalDays+1;

            var result = ObjectMapper.Map<LeaveInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            result.Status = "Pending";
            result.isManagerApprovalRequired = true;
            result.isHODApprovalRequired = false;
            result.isCEOApprovalRequired = false;

            if (leaveData.Name== "Medical" || leaveData.Name=="Casual")
            {
                if (date > 2)
                {
                    result.isHODApprovalRequired = true;
                    result.isCEOApprovalRequired = true;
                }
            }

             if (leaveData.Name == "Anual ")
            {
                if (date > 4)
                {
                    result.isHODApprovalRequired = true;
                    result.isCEOApprovalRequired = true;
                }
            }



            await _Leaverepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<LeaveDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_Leave_Update)]
        public override async Task<LeaveDto> UpdateAsync(LeaveDto input)
        {
            var data = ObjectMapper.Map<LeaveInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _Leaverepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<LeaveDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Leave_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _Leaverepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _Leaverepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Leave_Read)]
        [HttpPost]
        public List<LeaveDto> GetAllLeave()
        {
            var data = _Leaverepository.GetAll().Where(x => x.IsActive == true);
            return new List<LeaveDto>(ObjectMapper.Map<List<LeaveDto>>(data));
        }

       
            
    }
}
