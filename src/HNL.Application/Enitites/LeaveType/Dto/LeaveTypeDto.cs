﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.LeaveType.Dto
{
    [AutoMapTo(typeof(LeaveTypeInfo)), AutoMapFrom(typeof(LeaveTypeInfo))]

    public class LeaveTypeDto : EntityDto<long>
    {
        public string Name { get; set; }
        public int Count { get; set; }

    }
   
}
