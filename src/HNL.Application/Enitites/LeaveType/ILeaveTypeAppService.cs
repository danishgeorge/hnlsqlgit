﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.LeaveType.Dto;
using System.Collections.Generic;

namespace HNL.Entities.LeaveType
{
    public interface ILeaveTypeAppService : IAsyncCrudAppService< LeaveTypeDto, long, PagedResultRequestDto, LeaveTypeDto, LeaveTypeDto>
    {
        List<LeaveTypeDto> GetAllLeaveType();
        //Task<LeaveTypeEditDto> GetRoleForEdit(EntityDto input);
    }
}
