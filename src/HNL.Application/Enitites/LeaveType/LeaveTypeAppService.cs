﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.LeaveType.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.LeaveType
{
    [AbpAuthorize(PermissionNames.Pages_LeaveType)]
    public class LeaveTypeAppService : AsyncCrudAppService<LeaveTypeInfo, LeaveTypeDto, long, PagedResultRequestDto, LeaveTypeDto, LeaveTypeDto>, ILeaveTypeAppService
    {
        private readonly IRepository<LeaveTypeInfo, long> _leaveTypeRepository;
        private readonly IPermissionManager _permissionManager;

        public LeaveTypeAppService(IRepository<LeaveTypeInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _leaveTypeRepository = _repository;

            _permissionManager = _Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_LeaveType_Create)]
        public override async Task<LeaveTypeDto> CreateAsync(LeaveTypeDto input)
        {
            var result = ObjectMapper.Map<LeaveTypeInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;

            await _leaveTypeRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<LeaveTypeDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_LeaveType_Update)]
        public override async Task<LeaveTypeDto> UpdateAsync(LeaveTypeDto input)
        {
            var data = ObjectMapper.Map<LeaveTypeInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _leaveTypeRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<LeaveTypeDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_LeaveType_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _leaveTypeRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _leaveTypeRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_LeaveType_Read)]
        [HttpPost]
        public List<LeaveTypeDto> GetAllLeaveType()
        {
            var data = _leaveTypeRepository.GetAll().Where(x => x.IsActive == true);
            return new List<LeaveTypeDto>(ObjectMapper.Map<List<LeaveTypeDto>>(data));
        }

       
            
    }
}
