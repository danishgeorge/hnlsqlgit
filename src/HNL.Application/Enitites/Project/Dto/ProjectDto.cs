﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Project.Dto
{
    [AutoMapTo(typeof(ProjectInfo)), AutoMapFrom(typeof(ProjectInfo))]

    public class ProjectDto : EntityDto<long>
    {
        public string Name { get; set; }

    }
   
}
