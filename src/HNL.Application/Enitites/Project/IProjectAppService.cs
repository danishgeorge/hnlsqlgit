﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Project.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Project
{
    public interface IProjectAppService : IAsyncCrudAppService< ProjectDto, long, PagedResultRequestDto, ProjectDto, ProjectDto>
    {
        List<ProjectDto> GetAllProject();
        //Task<ProjectEditDto> GetRoleForEdit(EntityDto input);
    }
}
