﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Project.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Project
{   
    [AbpAuthorize(PermissionNames.Pages_Project)]
    public class ProjectAppService : AsyncCrudAppService<ProjectInfo, ProjectDto, long, PagedResultRequestDto, ProjectDto, ProjectDto>, IProjectAppService
    {
        private readonly IRepository<ProjectInfo, long> _projectRepository;
        private readonly IPermissionManager _permissionManager;

        public ProjectAppService(IRepository<ProjectInfo, long> _repository, IPermissionManager Manager) : base(_repository)
        {
            _projectRepository = _repository;
            _permissionManager = Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_Project_Create)]
        public override async Task<ProjectDto> CreateAsync(ProjectDto input)
        {
            var result = ObjectMapper.Map<ProjectInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;

            await _projectRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<ProjectDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_Project_Update)]
        public override async Task<ProjectDto> UpdateAsync(ProjectDto input)
        {
            var data = ObjectMapper.Map<ProjectInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _projectRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ProjectDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Project_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _projectRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _projectRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Project_Read)]
        [HttpPost]
        public List<ProjectDto> GetAllProject()
        {
            var data = _projectRepository.GetAll().Where(x => x.IsActive == true);
            return new List<ProjectDto>(ObjectMapper.Map<List<ProjectDto>>(data));
        }

       
            
    }
}
