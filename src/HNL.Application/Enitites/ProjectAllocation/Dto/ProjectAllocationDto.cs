﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.ProjectAllocation.Dto
{
    [AutoMapTo(typeof(ProjectAllocationInfo)), AutoMapFrom(typeof(ProjectAllocationInfo))]

    public class ProjectAllocationDto : EntityDto<long>
    {
        public string ErpId { get; set; }
        public long ProjectId { get; set; }

    }
   
}
