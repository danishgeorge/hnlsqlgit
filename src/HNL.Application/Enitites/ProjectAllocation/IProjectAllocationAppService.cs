﻿    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using HNL.Enitites.ProjectAllocation.Dto;
    using System.Collections.Generic;

    namespace HNL.Entities.ProjectAllocation
    {
        public interface IProjectAllocationAppService : IAsyncCrudAppService< ProjectAllocationDto, long, PagedResultRequestDto, ProjectAllocationDto, ProjectAllocationDto>
        {
            List<ProjectAllocationDto> GetAllProjectAllocation();
            //Task<ProjectAllocationEditDto> GetRoleForEdit(EntityDto input);
        }
    }
