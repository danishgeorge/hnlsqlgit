﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.RegionAllocation.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.RegionAllocation
{
    [AbpAuthorize(PermissionNames.Pages_RegionAllocation)]
    public class RegionAllocationAppService : AsyncCrudAppService<RegionAllocationInfo, RegionAllocationDto, long, PagedResultRequestDto, RegionAllocationDto, RegionAllocationDto>, IRegionAllocationAppService
    {
        private readonly IRepository<RegionAllocationInfo, long> _RegionAllocationRepository;
        private readonly IPermissionManager _permissionManager;

        public RegionAllocationAppService(IRepository<RegionAllocationInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _RegionAllocationRepository = _repository;
            _permissionManager = _Manager;
        }
        
        [AbpAuthorize(PermissionNames.Pages_RegionAllocation_Create)]
        public override async Task<RegionAllocationDto> CreateAsync(RegionAllocationDto input)
        {
            var result = ObjectMapper.Map<RegionAllocationInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;

            await _RegionAllocationRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<RegionAllocationDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_RegionAllocation_Update)]
        public override async Task<RegionAllocationDto> UpdateAsync(RegionAllocationDto input)
        {
            var data = ObjectMapper.Map<RegionAllocationInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _RegionAllocationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<RegionAllocationDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_RegionAllocation_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _RegionAllocationRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _RegionAllocationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_RegionAllocation_Read)]
        [HttpPost]
        public List<RegionAllocationDto> GetAllRegionAllocation()
        {
            var data = _RegionAllocationRepository.GetAll().Where(x => x.IsActive == true);
            return new List<RegionAllocationDto>(ObjectMapper.Map<List<RegionAllocationDto>>(data));
        }

       
            
    }
}
