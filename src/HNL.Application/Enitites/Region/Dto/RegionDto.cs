﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Region.Dto
{
    [AutoMapTo(typeof(RegionInfo)), AutoMapFrom(typeof(RegionInfo))]

    public class RegionDto : EntityDto<long>
    {
        public string Name { get; set; }

    }
   
}
