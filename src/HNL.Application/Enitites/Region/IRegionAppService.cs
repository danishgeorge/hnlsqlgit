﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Region.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Region
{
    public interface IRegionAppService : IAsyncCrudAppService< RegionDto, long, PagedResultRequestDto, RegionDto, RegionDto>
    {
        List<RegionDto> GetAllRegion();
        //Task<RegionEditDto> GetRoleForEdit(EntityDto input);
    }
}
