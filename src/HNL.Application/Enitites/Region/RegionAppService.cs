﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Region.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Region
{
    [AbpAuthorize(PermissionNames.Pages_Region)]
    public class RegionAppService : AsyncCrudAppService<RegionInfo, RegionDto, long, PagedResultRequestDto, RegionDto, RegionDto>, IRegionAppService
    {
        private readonly IRepository<RegionInfo, long> _Regionrepository;
        private readonly IPermissionManager _permissionManager;

        public RegionAppService(IRepository<RegionInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _Regionrepository = _repository;

            _permissionManager = _Manager;
        }
        [AbpAuthorize(PermissionNames.Pages_Region_Create)]
        public override async Task<RegionDto> CreateAsync(RegionDto input)
        {
            var result = ObjectMapper.Map<RegionInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;

            await _Regionrepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<RegionDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_Region_Update)]
        public override async Task<RegionDto> UpdateAsync(RegionDto input)
        {
            var data = ObjectMapper.Map<RegionInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _Regionrepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<RegionDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Region_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _Regionrepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _Regionrepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Region_Read)]
        [HttpPost]
        public List<RegionDto> GetAllRegion()
        {
            var data = _Regionrepository.GetAll().Where(x => x.IsActive == true);
            return new List<RegionDto>(ObjectMapper.Map<List<RegionDto>>(data));
        }

       
            
    }
}
