﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.RegionAllocation.Dto
{
    [AutoMapTo(typeof(RegionAllocationInfo)), AutoMapFrom(typeof(RegionAllocationInfo))]

    public class RegionAllocationDto : EntityDto<long>
    {
        public string ErpId { get; set; }
        public long RegionId { get; set; }

    }
   
}
