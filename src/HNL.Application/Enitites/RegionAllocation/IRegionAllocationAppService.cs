﻿    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using HNL.Enitites.RegionAllocation.Dto;
    using System.Collections.Generic;

    namespace HNL.Entities.RegionAllocation
    {
        public interface IRegionAllocationAppService : IAsyncCrudAppService< RegionAllocationDto, long, PagedResultRequestDto, RegionAllocationDto, RegionAllocationDto>
        {
            List<RegionAllocationDto> GetAllRegionAllocation();
            //Task<RegionAllocationEditDto> GetRoleForEdit(EntityDto input);
        }
    }
