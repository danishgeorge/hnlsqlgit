﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.ProjectAllocation.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.ProjectAllocation
{
    [AbpAuthorize(PermissionNames.Pages_ProjectAllocation)]
    public class ProjectAllocationAppService : AsyncCrudAppService<ProjectAllocationInfo, ProjectAllocationDto, long, PagedResultRequestDto, ProjectAllocationDto, ProjectAllocationDto>, IProjectAllocationAppService
    {
        private readonly IRepository<ProjectAllocationInfo, long> _ProjectAllocationRepository;
        private readonly IPermissionManager _permissionManager;

        public ProjectAllocationAppService(IRepository<ProjectAllocationInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _ProjectAllocationRepository = _repository;
            _permissionManager = _Manager;
        }
        
        [AbpAuthorize(PermissionNames.Pages_ProjectAllocation_Create)]
        public override async Task<ProjectAllocationDto> CreateAsync(ProjectAllocationDto input)
        {
            var result = ObjectMapper.Map<ProjectAllocationInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;

            await _ProjectAllocationRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<ProjectAllocationDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_ProjectAllocation_Update)]
        public override async Task<ProjectAllocationDto> UpdateAsync(ProjectAllocationDto input)
        {
            var data = ObjectMapper.Map<ProjectAllocationInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _ProjectAllocationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<ProjectAllocationDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_ProjectAllocation_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _ProjectAllocationRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _ProjectAllocationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_ProjectAllocation_Read)]
        [HttpPost]
        public List<ProjectAllocationDto> GetAllProjectAllocation()
        {
            var data = _ProjectAllocationRepository.GetAll().Where(x => x.IsActive == true);
            return new List<ProjectAllocationDto>(ObjectMapper.Map<List<ProjectAllocationDto>>(data));
        }

       
            
    }
}
