﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.Site.Dto
{
    [AutoMapTo(typeof(SiteInfo)), AutoMapFrom(typeof(SiteInfo))]

    public class SiteDto : EntityDto<long>
    {
    
        public string SiteName { get; set; }
        public string Region { get; set; }
        public string Severity { get; set; }
        public string SiteType { get; set; }
        public string SharingStatus { get; set; }
        public string SiteLat { get; set; }
        public string SiteLong { get; set; }
        public string Project { get; set; }
        public string SiteDescription { get; set; }
        public string Cluster { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
   
    }
}