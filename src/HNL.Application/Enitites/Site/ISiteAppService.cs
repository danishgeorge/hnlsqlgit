﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.Region.Dto;
using HNL.Enitites.Site.Dto;
using System.Collections.Generic;

namespace HNL.Entities.Site
{
    public interface ISiteAppService : IAsyncCrudAppService<SiteDto, long, PagedResultRequestDto, SiteDto, SiteDto>
    {
        List<SiteDto> GetAllSite();
        //Task<RegionEditDto> GetRoleForEdit(EntityDto input);
    }
}
