﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.UI;
using Abp.Web.Models;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.Cluster.Dto;
using HNL.Enitites.Country.Dto;
using HNL.Enitites.Project.Dto;
using HNL.Enitites.Region.Dto;
using HNL.Enitites.Site.Dto;
using HNL.Entities.City.Dto;
using HNL.MultiTenancy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.Site
{
    [AbpAuthorize(PermissionNames.Pages_Site)]

    public class SiteAppService : AsyncCrudAppService<SiteInfo, SiteDto, long, PagedResultRequestDto, SiteDto, SiteDto>, ISiteAppService
    {
        private readonly IRepository<SiteInfo, long> _siteRepository;
        private readonly IPermissionManager _permissionManager;
        private readonly IRepository<RegionInfo, long> _regionRepository;
        private readonly IRepository<CityInfo, long> _cityRepository;
        private readonly IRepository<ProjectInfo, long> _projectRepository;
        private readonly IRepository<CountryInfo, long> _countryRepository;
        private readonly IRepository<ClusterInfo, long> _clusterRepository;

       


        public SiteAppService(IRepository<SiteInfo, long> _repository,
            IRepository<RegionInfo, long> regionrepository, IRepository<CityInfo, long> cityrepository,
            IRepository<CountryInfo, long> countryrepository,
            IRepository<ClusterInfo, long> clusterRepository,

            IRepository<ProjectInfo, long> projectrepository, IPermissionManager _Manager) : base(_repository)
          
        {
            _siteRepository = _repository;
            _permissionManager = _Manager;
            _regionRepository = regionrepository;
            _cityRepository = cityrepository;
            _countryRepository = countryrepository;
            _projectRepository = projectrepository;
            _clusterRepository = clusterRepository;

            
        }

        [AbpAuthorize(PermissionNames.Pages_Site_Create)]
        public override async Task<SiteDto> CreateAsync(SiteDto input)
        {

            var datasite = _siteRepository.GetAll().Where(x => x.SiteName == input.SiteName && x.TenantId == AbpSession.TenantId).FirstOrDefault();
            if (datasite != null) {
                throw new UserFriendlyException("There is already site entered with that name");
            }
            ///
            SiteDto site = new SiteDto();
            
            
                try
                {
                var dataRegion = _regionRepository.GetAll().Where(x => x.Name == input.Region && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                if (dataRegion == null)
                {
                    RegionDto regionDto = new RegionDto();
                    regionDto.Name = input.Region;
                    var resultR = ObjectMapper.Map<RegionInfo>(regionDto);
                    resultR.CreationTime = DateTime.Now;
                    resultR.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    resultR.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultR.IsActive = true;
                    await _regionRepository.InsertAsync(resultR);
                    CurrentUnitOfWork.SaveChanges();
                    dataRegion = resultR;

                }
                    // Cluster
                    var dataCluster = _clusterRepository.GetAll().Where(x => x.Name == input.Cluster && x.TenantId == AbpSession.TenantId).FirstOrDefault();

                    if (dataCluster == null)
                    {
                        ClusterDto clusterDto = new ClusterDto();
                        clusterDto.Name = input.Cluster;
                        var resultR = ObjectMapper.Map<ClusterInfo>(clusterDto);
                        resultR.CreationTime = DateTime.Now;
                        resultR.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultR.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultR.IsActive = true;
                        await _clusterRepository.InsertAsync(resultR);
                        CurrentUnitOfWork.SaveChanges();
                        dataCluster = resultR;

                    }


                    var dataCity = _cityRepository.GetAll().Where(x => x.Name == input.City && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                    if (dataCity == null)
                    {
                        CityDto cityDto = new CityDto();
                        cityDto.Name = input.City;
                        var resultCity = ObjectMapper.Map<CityInfo>(cityDto);
                        resultCity.CreationTime = DateTime.Now;
                        resultCity.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultCity.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultCity.IsActive = true;
                        await _cityRepository.InsertAsync(resultCity);
                        CurrentUnitOfWork.SaveChanges();
                        dataCity = resultCity;
                    }

                    var dataProject = _projectRepository.GetAll().Where(x => x.Name == input.Project && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                    if (dataProject == null)
                    {
                        ProjectDto projectDto = new ProjectDto();
                        projectDto.Name = input.Project;
                        var resultP = ObjectMapper.Map<ProjectInfo>(projectDto);
                        resultP.CreationTime = DateTime.Now;
                        resultP.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultP.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultP.IsActive = true;
                        await _projectRepository.InsertAsync(resultP);
                        CurrentUnitOfWork.SaveChanges();
                        dataProject = resultP;
                    }
                    var dataCountry = _countryRepository.GetAll().Where(x => x.Name == input.Country && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                    if (dataCountry == null)
                    {
                        CountryDto countryDto = new CountryDto();
                        countryDto.Name = input.Country;
                        var resultCo = ObjectMapper.Map<CountryInfo>(countryDto);
                        resultCo.CreationTime = DateTime.Now;
                        resultCo.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                        resultCo.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    resultCo.IsActive = true;
                        await _countryRepository.InsertAsync(resultCo);
                        CurrentUnitOfWork.SaveChanges();
                        dataCountry = resultCo;
                    }
                    //var dataVendor = _vendorRepository.GetAll().Where(x => x.Name == input.Vendor && x.TenantId == AbpSession.TenantId).FirstOrDefault();
                    //if (dataVendor == null)
                    //{
                    //    VendorDto vendorDto = new VendorDto();
                    //    vendorDto.Name = input.Vendor;
                    //    var resultV = ObjectMapper.Map<VendorInfo>(vendorDto);
                    //    resultV.CreationTime = DateTime.Now;
                    //    resultV.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    //    resultV.TenantId = Convert.ToInt32(AbpSession.TenantId);
                    //resultV.IsActive = true;
                    //    await _vendorRepository.InsertAsync(resultV);
                    //    CurrentUnitOfWork.SaveChanges();
                    //    dataVendor = resultV;
                    //}

                    var result = ObjectMapper.Map<SiteInfo>(input);
                    result.RegionId = dataRegion.Id;
                    result.CityId = dataCity.Id;
                    result.ProjectId = dataProject.Id;
                    result.ClusterId = dataCluster.Id;
                    result.CountryId = dataCountry.Id;
                    result.CreationTime = DateTime.Now;
                    result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
                    result.TenantId = Convert.ToInt32(AbpSession.TenantId);
                result.IsActive = true;
                    await _siteRepository.InsertAsync(result);
                    CurrentUnitOfWork.SaveChanges();
                    var data = result.MapTo<SiteDto>();
                    return data;

                }
                catch (Exception ex)
                {

                }
            
            

            return site;
           
            
        }

        [AbpAuthorize(PermissionNames.Pages_Site_Update)]
        public override async Task<SiteDto> UpdateAsync(SiteDto input)
        {
            var data = ObjectMapper.Map<SiteInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _siteRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<SiteDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_Site_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _siteRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _siteRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_Site_Read)]
        [HttpPost]
        public List<SiteDto> GetAllSite()
        {
            var data = _siteRepository.GetAll().Where(x => x.IsActive == true);
            return new List<SiteDto>(ObjectMapper.Map<List<SiteDto>>(data));
        }



    }
}
