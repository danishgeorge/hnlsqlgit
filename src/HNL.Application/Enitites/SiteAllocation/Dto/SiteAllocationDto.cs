﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using HNL.Enitites;
using Abp.Application.Services.Dto;

namespace HNL.Enitites.SiteAllocation.Dto
{
    [AutoMapTo(typeof(SiteAllocationInfo)), AutoMapFrom(typeof(SiteAllocationInfo))]

    public class SiteAllocationDto : EntityDto<long>
    {
       
        public string ErpId { get; set; }
        public long SiteId { get; set; }

    }
   
}
