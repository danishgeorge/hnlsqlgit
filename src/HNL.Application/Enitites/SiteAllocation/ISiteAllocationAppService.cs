﻿    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using HNL.Enitites.SiteAllocation.Dto;
    using System.Collections.Generic;

    namespace HNL.Entities.SiteAllocation
    {
        public interface ISiteAllocationAppService : IAsyncCrudAppService< SiteAllocationDto, long, PagedResultRequestDto, SiteAllocationDto, SiteAllocationDto>
        {
            List<SiteAllocationDto> GetAllSiteAllocation();
            //Task<SiteAllocationEditDto> GetRoleForEdit(EntityDto input);
        }
    }
