﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.SiteAllocation.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.SiteAllocation
{
    [AbpAuthorize(PermissionNames.Pages_SiteAllocation)]
    public class SiteAllocationAppService : AsyncCrudAppService<SiteAllocationInfo, SiteAllocationDto, long, PagedResultRequestDto, SiteAllocationDto, SiteAllocationDto>, ISiteAllocationAppService
    {
        private readonly IRepository<SiteAllocationInfo, long> _siteAllocationRepository;
        private readonly IPermissionManager _permissionManager;

        public SiteAllocationAppService(IRepository<SiteAllocationInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _siteAllocationRepository = _repository;
            _permissionManager = _Manager;
        }
        
        [AbpAuthorize(PermissionNames.Pages_SiteAllocation_Create)]
        public override async Task<SiteAllocationDto> CreateAsync(SiteAllocationDto input)
        {
            var result = ObjectMapper.Map<SiteAllocationInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;


            await _siteAllocationRepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<SiteAllocationDto>();
            return data;

        }
        [AbpAuthorize(PermissionNames.Pages_SiteAllocation_Update)]
        public override async Task<SiteAllocationDto> UpdateAsync(SiteAllocationDto input)
        {
            var data = ObjectMapper.Map<SiteAllocationInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _siteAllocationRepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<SiteAllocationDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_SiteAllocation_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _siteAllocationRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _siteAllocationRepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }
        [AbpAuthorize(PermissionNames.Pages_SiteAllocation_Read)]
        [HttpPost]
        public List<SiteAllocationDto> GetAllSiteAllocation()
        {
            var data = _siteAllocationRepository.GetAll().Where(x => x.IsActive == true);
            return new List<SiteAllocationDto>(ObjectMapper.Map<List<SiteAllocationDto>>(data));
        }

       
            
    }
}
