﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HNL.Enitites.UserAttendance.Dto
{
    [AutoMapTo(typeof(UserAttendanceInfo)), AutoMapFrom(typeof(UserAttendanceInfo))]

    public class UserCheckInDto : EntityDto<long>
    {
        public string ErpId { get; set; }
        public string StartLat { get; set; }
        public string StartLong { get; set; }
        public string CheckInFrom { get; set; }

    }
}

