﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HNL.Enitites.UserAttendance.Dto
{
    [AutoMapTo(typeof(UserAttendanceInfo)), AutoMapFrom(typeof(UserAttendanceInfo))]

    public class UserCheckOutDto 
    {
        public long Id { get; set; }
        public string EndLat { get; set; }
        public string EndLong { get; set; }
        public string CheckOutFrom { get; set; }

    }
}

