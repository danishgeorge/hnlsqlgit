﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Enitites.UserAttendance.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HNL.Enitites.UserAttendance
{
    public interface IUserAttendanceAppService : IAsyncCrudAppService<UserAttendanceDto, long, PagedResultRequestDto, UserAttendanceDto, UserAttendanceDto>
    {
        List<UserAttendanceDto> GetAllUserAttendance();
        //Task<RegionEditDto> GetRoleForEdit(EntityDto input);

        // checkIn
         UserAttendanceDto UserCheckIn(UserCheckInDto userCheckInDto);
    }
}
