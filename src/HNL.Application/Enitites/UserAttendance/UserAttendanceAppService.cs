﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using HNL.Authorization;
using HNL.Enitites;
using HNL.Enitites.UserAttendance;
using HNL.Enitites.UserAttendance.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HNL.Entities.UserAttendance
{
    [AbpAuthorize(PermissionNames.Pages_UserAttendance)]
    public class UserAttendanceAppService : AsyncCrudAppService<UserAttendanceInfo, UserAttendanceDto, long, PagedResultRequestDto, UserAttendanceDto, UserAttendanceDto>, IUserAttendanceAppService
    {
        private readonly IRepository<UserAttendanceInfo, long> _UserAttendancerepository;
        private readonly IPermissionManager _permissionManager;

        public UserAttendanceAppService(IRepository<UserAttendanceInfo, long> _repository, IPermissionManager _Manager) : base(_repository)
        {
            _UserAttendancerepository = _repository;

            _permissionManager = _Manager;
        }


        //----------> UserCheckIN
        public   UserAttendanceDto UserCheckIn(UserCheckInDto userCheckInDto)
        {

            var result = ObjectMapper.Map<UserAttendanceInfo>(userCheckInDto);
            result.CheckInTime = DateTime.Now;
            result.Date = DateTime.Now.Date;
            
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.TenantId);
            result.IsActive = true;
             _UserAttendancerepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserAttendanceDto>();
            return data;
        }

        //----------> Get Last Attendance Entry
        [HttpGet]
        public UserAttendanceInfo GetLastAttendanceEntryByErpId(string ErpId)
        {
            var data = _UserAttendancerepository.GetAll()
                .Where(x => x.IsActive == true && x.ErpId==ErpId).OrderByDescending(x=>x.Id).First();
            var result = ObjectMapper.Map<UserAttendanceInfo>(data);
            return result;
        }



        //----------> UserCheckOut
        public async Task<UserAttendanceDto> UserCheckOutAsync(UserCheckOutDto userCheckOutDto)
        {
            var input = _UserAttendancerepository.GetAll().Where(x => x.IsActive == true && x.Id== userCheckOutDto.Id).FirstOrDefault();
            var data = ObjectMapper.Map<UserAttendanceInfo>(input);
            data.CheckOutTime= DateTime.Now;
            data.CheckOutFrom = userCheckOutDto.CheckOutFrom;
            data.EndLat = userCheckOutDto.EndLat;
            data.EndLong = userCheckOutDto.EndLong;
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _UserAttendancerepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserAttendanceDto>();
            return result;


        }

        [AbpAuthorize(PermissionNames.Pages_UserAttendance_Create)]
        public override async Task<UserAttendanceDto> CreateAsync(UserAttendanceDto input)
        {
            var result = ObjectMapper.Map<UserAttendanceInfo>(input);
            result.CreationTime = DateTime.Now;
            result.CreatorUserId = Convert.ToInt32(AbpSession.UserId);
            result.TenantId = Convert.ToInt32(AbpSession.UserId);
            result.IsActive = true;
            await _UserAttendancerepository.InsertAsync(result);
            CurrentUnitOfWork.SaveChanges();

            var data = result.MapTo<UserAttendanceDto>();
            return data;

        }


        [AbpAuthorize(PermissionNames.Pages_UserAttendance_Update)]
        public override async Task<UserAttendanceDto> UpdateAsync(UserAttendanceDto input)
        {
            var data = ObjectMapper.Map<UserAttendanceInfo>(input);
            data.LastModificationTime = DateTime.Now;
            data.LastModifierUserId = Convert.ToInt32(AbpSession.UserId);

            await _UserAttendancerepository.UpdateAsync(data);
            CurrentUnitOfWork.SaveChanges();

            var result = data.MapTo<UserAttendanceDto>();
            return result;
        }
        [AbpAuthorize(PermissionNames.Pages_UserAttendance_Delete)]
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var result = _UserAttendancerepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault();
            if (result != null)
            {
                await _UserAttendancerepository.DeleteAsync(result);
                CurrentUnitOfWork.SaveChanges();
            }

        }

        [AbpAuthorize(PermissionNames.Pages_UserAttendance_Read)]
        [HttpPost]
        public List<UserAttendanceDto> GetAllUserAttendance()
        {
            var data = _UserAttendancerepository.GetAll().Where(x => x.IsActive == true);
            return new List<UserAttendanceDto>(ObjectMapper.Map<List<UserAttendanceDto>>(data));
        }

      
    }
}
