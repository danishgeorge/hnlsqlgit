﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using HNL.Authorization;

namespace HNL
{
    [DependsOn(
        typeof(HNLCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class HNLApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<HNLAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(HNLApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
