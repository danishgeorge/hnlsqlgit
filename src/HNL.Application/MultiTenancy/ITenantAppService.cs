﻿using Abp.Application.Services;
using HNL.MultiTenancy.Dto;

namespace HNL.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

