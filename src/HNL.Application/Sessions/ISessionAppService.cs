﻿using System.Threading.Tasks;
using Abp.Application.Services;
using HNL.Sessions.Dto;

namespace HNL.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
