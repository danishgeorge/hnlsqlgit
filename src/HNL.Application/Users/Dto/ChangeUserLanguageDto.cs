using System.ComponentModel.DataAnnotations;

namespace HNL.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}