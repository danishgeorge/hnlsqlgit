using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using HNL.Roles.Dto;
using HNL.Users.Dto;

namespace HNL.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);

        Task<bool> ChangePassword(ChangePasswordDto input);
    }
}
