﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace HNL.Authorization
{
    public class HNLAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            //Cities Permission
            context.CreatePermission(PermissionNames.Pages_City, L("City"));
            context.CreatePermission(PermissionNames.Pages_City_Create, L("CreateCity"));
            context.CreatePermission(PermissionNames.Pages_City_Read, L("EditCity"));
            context.CreatePermission(PermissionNames.Pages_City_Update, L("UpdateCity"));
            context.CreatePermission(PermissionNames.Pages_City_Delete, L("DeleteCity"));
            //Sites Permission
            context.CreatePermission(PermissionNames.Pages_Site, L("Site"));
            context.CreatePermission(PermissionNames.Pages_Site_Create, L("CreateSite"));
            context.CreatePermission(PermissionNames.Pages_Site_Read, L("EditSite"));
            context.CreatePermission(PermissionNames.Pages_Site_Update, L("UpdateSite"));
            context.CreatePermission(PermissionNames.Pages_Site_Delete, L("DeleteSite"));
            //Country Permission
            context.CreatePermission(PermissionNames.Pages_Country, L("Country"));
            context.CreatePermission(PermissionNames.Pages_Country_Create, L("CreateCountry"));
            context.CreatePermission(PermissionNames.Pages_Country_Read, L("EditCountry"));
            context.CreatePermission(PermissionNames.Pages_Country_Update, L("UpdateCountry"));
            context.CreatePermission(PermissionNames.Pages_Country_Delete, L("DeleteCountry"));
            //Employee Permission
            context.CreatePermission(PermissionNames.Pages_Employee, L("Employee"));
            context.CreatePermission(PermissionNames.Pages_Employee_Create, L("CreateEmployee"));
            context.CreatePermission(PermissionNames.Pages_Employee_Read, L("EditEmployee"));
            context.CreatePermission(PermissionNames.Pages_Employee_Update, L("UpdateEmployee"));
            context.CreatePermission(PermissionNames.Pages_Employee_Delete, L("DeleteEmployee"));

            //SiteAllocation Permission
            context.CreatePermission(PermissionNames.Pages_SiteAllocation, L("SiteAllocation"));
            context.CreatePermission(PermissionNames.Pages_SiteAllocation_Create, L("CreateSiteAllocation"));
            context.CreatePermission(PermissionNames.Pages_SiteAllocation_Read, L("EditSiteAllocation"));
            context.CreatePermission(PermissionNames.Pages_SiteAllocation_Update, L("UpdateSiteAllocation"));
            context.CreatePermission(PermissionNames.Pages_SiteAllocation_Delete, L("DeleteSiteAllocation"));

            //Vendor Permission
            context.CreatePermission(PermissionNames.Pages_Vendor, L("Vendor"));
            context.CreatePermission(PermissionNames.Pages_Vendor_Create, L("CreateVendor"));
            context.CreatePermission(PermissionNames.Pages_Vendor_Read, L("EditVendor"));
            context.CreatePermission(PermissionNames.Pages_Vendor_Update, L("UpdateVendor"));
            context.CreatePermission(PermissionNames.Pages_Vendor_Delete, L("DeleteVendor"));
            //Project Permission
            context.CreatePermission(PermissionNames.Pages_Project, L("Project"));
            context.CreatePermission(PermissionNames.Pages_Project_Create, L("CreateProject"));
            context.CreatePermission(PermissionNames.Pages_Project_Read, L("EditProject"));
            context.CreatePermission(PermissionNames.Pages_Project_Update, L("UpdateProject"));
            context.CreatePermission(PermissionNames.Pages_Project_Delete, L("DeleteProject"));
            //Region Permission
            context.CreatePermission(PermissionNames.Pages_Region, L("Region"));
            context.CreatePermission(PermissionNames.Pages_Region_Create, L("CreateRegion"));
            context.CreatePermission(PermissionNames.Pages_Region_Read, L("EditRegion"));
            context.CreatePermission(PermissionNames.Pages_Region_Update, L("UpdateRegion"));
            context.CreatePermission(PermissionNames.Pages_Region_Delete, L("DeleteRegion"));

            //Department Permission
            context.CreatePermission(PermissionNames.Pages_Department, L("Department"));
            context.CreatePermission(PermissionNames.Pages_Department_Create, L("CreateDepartment"));
            context.CreatePermission(PermissionNames.Pages_Department_Read, L("EditDepartment"));
            context.CreatePermission(PermissionNames.Pages_Department_Update, L("UpdateDepartment"));
            context.CreatePermission(PermissionNames.Pages_Department_Delete, L("DeleteDepartment"));

            //Designation Permission
            context.CreatePermission(PermissionNames.Pages_Designation, L("Designation"));
            context.CreatePermission(PermissionNames.Pages_Designation_Create, L("CreateDesignation"));
            context.CreatePermission(PermissionNames.Pages_Designation_Read, L("EditDesignation"));
            context.CreatePermission(PermissionNames.Pages_Designation_Update, L("UpdateDesignation"));
            context.CreatePermission(PermissionNames.Pages_Designation_Delete, L("DeleteDesignation"));

            //Designation Permission
            context.CreatePermission(PermissionNames.Pages_UserAttendance, L("UserAttendance"));
            context.CreatePermission(PermissionNames.Pages_UserAttendance_Create, L("CreateUserAttendance"));
            context.CreatePermission(PermissionNames.Pages_UserAttendance_Read, L("EditUserAttendance"));
            context.CreatePermission(PermissionNames.Pages_UserAttendance_Update, L("UpdateUserAttendance"));
            context.CreatePermission(PermissionNames.Pages_UserAttendance_Delete, L("DeleteUserAttendance"));
            //Designation Permission
            context.CreatePermission(PermissionNames.Pages_Leave, L("Leave"));
            context.CreatePermission(PermissionNames.Pages_Leave_Create, L("CreateLeave"));
            context.CreatePermission(PermissionNames.Pages_Leave_Read, L("EditLeave"));
            context.CreatePermission(PermissionNames.Pages_Leave_Update, L("UpdateLeave"));
            context.CreatePermission(PermissionNames.Pages_Leave_Delete, L("DeleteLeave"));
            //Designation Permission
            context.CreatePermission(PermissionNames.Pages_LeaveType, L("LeaveType"));
            context.CreatePermission(PermissionNames.Pages_LeaveType_Create, L("CreateLeaveType"));
            context.CreatePermission(PermissionNames.Pages_LeaveType_Read, L("EditLeaveType"));
            context.CreatePermission(PermissionNames.Pages_LeaveType_Update, L("UpdateLeaveType"));
            context.CreatePermission(PermissionNames.Pages_LeaveType_Delete, L("DeleteLeaveType"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, HNLConsts.LocalizationSourceName);
        }
    }
}
