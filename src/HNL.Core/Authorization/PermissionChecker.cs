﻿using Abp.Authorization;
using HNL.Authorization.Roles;
using HNL.Authorization.Users;

namespace HNL.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
