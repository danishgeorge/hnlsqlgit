﻿namespace HNL.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";


        //City API permissions
        public const string Pages_City = "Pages.City";
        public const string Pages_City_Create = "Pages.City.Create";
        public const string Pages_City_Read = "Pages.City.Read";
        public const string Pages_City_Update = "Pages.City.Update";
        public const string Pages_City_Delete = "Pages.City.Delete";


        //Site API permissions
        public const string Pages_Site = "Pages.Site";
        public const string Pages_Site_Create = "Pages.Site.Create";
        public const string Pages_Site_Read = "Pages.Site.Read";
        public const string Pages_Site_Update = "Pages.Site.Update";
        public const string Pages_Site_Delete = "Pages.Site.Delete";
        // Country API permissions
        public const string Pages_Country = "Pages.Country";
        public const string Pages_Country_Create = "Pages.Country.Create";
        public const string Pages_Country_Read = "Pages.Country.Read";
        public const string Pages_Country_Update = "Pages.Country.Update";
        public const string Pages_Country_Delete = "Pages.Country.Delete";
        // Department API permissions
        public const string Pages_Department = "Pages.Department";
        public const string Pages_Department_Create = "Pages.Department.Create";
        public const string Pages_Department_Read = "Pages.Department.Read";
        public const string Pages_Department_Update = "Pages.Department.Update";
        public const string Pages_Department_Delete = "Pages.Department.Delete";

        // Designation API permissions
        public const string Pages_Designation = "Pages.Designation";
        public const string Pages_Designation_Create = "Pages.Designation.Create";
        public const string Pages_Designation_Read = "Pages.Designation.Read";
        public const string Pages_Designation_Update = "Pages.Designation.Update";
        public const string Pages_Designation_Delete = "Pages.Designation.Delete";

        // Project API permissions
        public const string Pages_Project = "Pages.Project";
        public const string Pages_Project_Create = "Pages.Project.Create";
        public const string Pages_Project_Read = "Pages.Project.Read";
        public const string Pages_Project_Update = "Pages.Project.Update";
        public const string Pages_Project_Delete = "Pages.Project.Delete";
        // Region API permissions
        public const string Pages_Region = "Pages.Region";
        public const string Pages_Region_Create = "Pages.Region.Create";
        public const string Pages_Region_Read = "Pages.Region.Read";
        public const string Pages_Region_Update = "Pages.Region.Update";
        public const string Pages_Region_Delete = "Pages.Region.Delete";
        // Employee API permissions
        public const string Pages_Employee = "Pages.Employee";
        public const string Pages_Employee_Create = "Pages.Employee.Create";
        public const string Pages_Employee_Read = "Pages.Employee.Read";
        public const string Pages_Employee_Update = "Pages.Employee.Update";
        public const string Pages_Employee_Delete = "Pages.Employee.Delete";
        // Vendor API permissions
        public const string Pages_Vendor = "Pages.Vendor";
        public const string Pages_Vendor_Create = "Pages.Vendor.Create";
        public const string Pages_Vendor_Read = "Pages.Vendor.Read";
        public const string Pages_Vendor_Update = "Pages.Vendor.Update";
        public const string Pages_Vendor_Delete = "Pages.Vendor.Delete";

        // SiteAllocation API permissions
        public const string Pages_SiteAllocation = "Pages.SiteAllocation";
        public const string Pages_SiteAllocation_Create = "Pages.SiteAllocation.Create";
        public const string Pages_SiteAllocation_Read = "Pages.SiteAllocation.Read";
        public const string Pages_SiteAllocation_Update = "Pages.SiteAllocation.Update";
        public const string Pages_SiteAllocation_Delete = "Pages.SiteAllocation.Delete";

        // Cluster API permissions
        public const string Pages_Cluster = "Pages.Cluster";
        public const string Pages_Cluster_Create = "Pages.Cluster.Create";
        public const string Pages_Cluster_Read = "Pages.Cluster.Read";
        public const string Pages_Cluster_Update = "Pages.Cluster.Update";
        public const string Pages_Cluster_Delete = "Pages.Cluster.Delete";

        // UserAttendance API permissions
        public const string Pages_UserAttendance = "Pages.UserAttendance";
        public const string Pages_UserAttendance_Create = "Pages.UserAttendance.Create";
        public const string Pages_UserAttendance_Read = "Pages.UserAttendance.Read";
        public const string Pages_UserAttendance_Update = "Pages.UserAttendance.Update";
        public const string Pages_UserAttendance_Delete = "Pages.UserAttendance.Delete";

        // ProjectAllocation API permissions
        public const string Pages_ProjectAllocation = "Pages.ProjectAllocation";
        public const string Pages_ProjectAllocation_Create = "Pages.ProjectAllocation.Create";
        public const string Pages_ProjectAllocation_Read = "Pages.ProjectAllocation.Read";
        public const string Pages_ProjectAllocation_Update = "Pages.ProjectAllocation.Update";
        public const string Pages_ProjectAllocation_Delete = "Pages.ProjectAllocation.Delete";


        // RegionAllocation API permissions
        public const string Pages_RegionAllocation = "Pages.RegionAllocation";
        public const string Pages_RegionAllocation_Create = "Pages.RegionAllocation.Create";
        public const string Pages_RegionAllocation_Read = "Pages.RegionAllocation.Read";
        public const string Pages_RegionAllocation_Update = "Pages.RegionAllocation.Update";
        public const string Pages_RegionAllocation_Delete = "Pages.RegionAllocation.Delete";




        // ClusterAllocation API permissions
        public const string Pages_ClusterAllocation = "Pages.ClusterAllocation";
        public const string Pages_ClusterAllocation_Create = "Pages.ClusterAllocation.Create";
        public const string Pages_ClusterAllocation_Read = "Pages.ClusterAllocation.Read";
        public const string Pages_ClusterAllocation_Update = "Pages.ClusterAllocation.Update";
        public const string Pages_ClusterAllocation_Delete = "Pages.ClusterAllocation.Delete";


        // LeaveType API permissions
        public const string Pages_LeaveType = "Pages.LeaveType";
        public const string Pages_LeaveType_Create = "Pages.LeaveType.Create";
        public const string Pages_LeaveType_Read = "Pages.LeaveType.Read";
        public const string Pages_LeaveType_Update = "Pages.LeaveType.Update";
        public const string Pages_LeaveType_Delete = "Pages.LeaveType.Delete";

        // LeaveAPI permissions
        public const string Pages_Leave = "Pages.Leave";
        public const string Pages_Leave_Create = "Pages.Leave.Create";
        public const string Pages_Leave_Read = "Pages.Leave.Read";
        public const string Pages_Leave_Update = "Pages.Leave.Update";
        public const string Pages_Leave_Delete = "Pages.Leave.Delete";
    }
}


