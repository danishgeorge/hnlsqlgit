﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HNL.Enitites
{
   public class ClusterAllocationInfo : HNLProjBaseEntity
    {
        public string ErpId { get; set; }


        [ForeignKey("ClusterInfo")]
        public long ClusterId { get; set; }
        public virtual ClusterInfo ClusterInfo { get; set; }
    }
}
