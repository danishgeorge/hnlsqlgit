﻿using HNL.Authorization.Roles;
using HNL.Authorization.Users;
using HNL.MultiTenancy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HNL.Enitites
{
   public class EmployeeInfo : HNLProjBaseEntity
    {

        public string ErpId { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string CNIC { get; set; }

        [ForeignKey("DesignationInfo")]
        public long DesignationInfoId { get; set; }
        public virtual DesignationInfo DesignationInfo { get; set; }

        [ForeignKey("CityInfo")]
        public long CityInfoId { get; set; }
        public virtual CityInfo CityInfo { get; set; }

        [ForeignKey("CountryInfo")]
        public long CountryInfoId { get; set; }
        public virtual CountryInfo CountryInfo { get; set; }

        [ForeignKey("DepartmentInfo")]   
        public long DepartmentInfoId { get; set; }
        public virtual DepartmentInfo DepartmentInfo { get; set; }

        [ForeignKey("UserId")]
        public long UserId { get; set; }
        public virtual User User { get; set; }

        public string CellNo { get; set; }
        public string Loc { get; set; }
        public string DeviceToken { get; set; }
        public string ProfileImage { get; set; }

    }
}
