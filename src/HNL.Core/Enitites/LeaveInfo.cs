﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HNL.Enitites
{
    public class LeaveInfo : HNLProjBaseEntity
    {
        [ForeignKey("LeaveTypeInfo")]
        public long LeaveTypeId { get; set; }
        public virtual LeaveTypeInfo LeaveTypeInfo { get; set; }
        public string ErpId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool isManagerApprovalRequired { get; set; }
        public bool isManagerApproved { get; set; }
        public bool isHODApprovalRequired { get; set; }
        public bool isHODApproved { get; set; }
        public bool isCEOApprovalRequired { get; set; }
        public bool isCEOApproved { get; set; }
    }
}
