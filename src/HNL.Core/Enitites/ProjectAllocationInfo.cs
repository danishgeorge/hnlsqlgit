﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HNL.Enitites
{
    public class ProjectAllocationInfo : HNLProjBaseEntity
    {
        public string ErpId { get; set; }
       
       
        [ForeignKey("ProjectInfo")]
        public long ProjectId { get; set; }
        public virtual ProjectInfo ProjectInfo { get; set; }

    }
}
