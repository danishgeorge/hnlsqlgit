﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HNL.Enitites
{
    public class RegionAllocationInfo : HNLProjBaseEntity
    {
        public string ErpId { get; set; }
      
        
        [ForeignKey("RegionInfo")]
        public long RegionId { get; set; }
        public virtual RegionInfo RegionInfo { get; set; }


    }
}
