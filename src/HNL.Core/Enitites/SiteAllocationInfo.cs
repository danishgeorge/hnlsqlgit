﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HNL.Enitites
{
   public class SiteAllocationInfo : HNLProjBaseEntity
    {
        public string ErpId { get; set; }


        [ForeignKey("SiteInfo")]
        public long SiteInfoId { get; set; }
        public virtual SiteInfo SiteInfo { get; set; }
  
    }
}