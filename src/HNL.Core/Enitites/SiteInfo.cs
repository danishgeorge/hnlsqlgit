﻿using HNL.MultiTenancy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HNL.Enitites
{
    public class SiteInfo : HNLProjBaseEntity
    {
        [ForeignKey("CityInfo")]
        public long CityId { get; set; }
        public virtual CityInfo CityInfo { get; set; }


        [ForeignKey("CountryInfo")]
        public long CountryId { get; set; }
        public virtual CountryInfo CountryInfo { get; set; }

        

        [ForeignKey("ProjectInfo")]
        public long ProjectId { get; set; }
        public virtual ProjectInfo ProjectInfo { get; set; }

        [ForeignKey("RegionInfo")]
        public long RegionId { get; set; }
        public virtual RegionInfo RegionInfo { get; set; }

        [ForeignKey("ClusterInfo")]
        public long ClusterId { get; set; }
        public virtual ClusterInfo ClusterInfo { get; set; }


        public string SiteName { get; set; }
        public string Severity { get; set; }
        public string SiteType { get; set; }
        
        public string SharingStatus { get; set; }
        
        public string SiteLat { get; set; }
        public string SiteLong { get; set; }
        public string SiteDescription { get; set; }
    }
}
