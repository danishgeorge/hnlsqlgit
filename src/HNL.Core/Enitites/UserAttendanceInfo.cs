﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HNL.Enitites
{
   public  class UserAttendanceInfo : HNLProjBaseEntity
    {
        public string ErpId { get; set; }
        public DateTime Date { get; set; }
        public DateTime CheckInTime { get; set; }
        public DateTime CheckOutTime { get; set; }
        public string StartLat { get; set; }
        public string StartLong { get; set; }
        public string EndLat { get; set; }
        public string EndLong { get; set; }
        public string CheckInFrom { get; set; }
        public string CheckOutFrom { get; set; }
    }
}
