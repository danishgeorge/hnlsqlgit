﻿namespace HNL
{
    public class HNLConsts
    {
        public const string LocalizationSourceName = "HNL";

        public const string ConnectionStringName = "Default";   

        public const bool MultiTenancyEnabled = true;
    }
}
