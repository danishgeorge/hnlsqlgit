﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace HNL
{
    public class HNLProjBaseEntity : FullAuditedEntity<long>
    {
        public bool IsActive { get; set; }
        public int TenantId { get; set; }
    
    }
}
