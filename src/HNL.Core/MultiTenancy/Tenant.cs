﻿using Abp.MultiTenancy;
using HNL.Authorization.Users;

namespace HNL.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {

        }
    }
}
