﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using HNL.Authorization.Roles;
using HNL.Authorization.Users;
using HNL.MultiTenancy;
using HNL.Enitites;

namespace HNL.EntityFrameworkCore
{
    public class HNLDbContext : AbpZeroDbContext<Tenant, Role, User, HNLDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public HNLDbContext(DbContextOptions<HNLDbContext> options)
            : base(options)
        {
        }
        public DbSet<CityInfo> CityInfo { get; set; }
        public DbSet<CountryInfo> CountryInfo { get; set; }
        public DbSet<DepartmentInfo> DepartmentInfo { get; set; }
        public DbSet<DesignationInfo> DesignationInfo { get; set; }
        public DbSet<EmployeeInfo> EmployeeInfo { get; set; }
        public DbSet<ProjectInfo> ProjectInfo { get; set; }
        public DbSet<RegionInfo> RegionInfo { get; set; }
        public DbSet<ClusterInfo> ClusterInfo { get; set; }
        public DbSet<SiteInfo> SiteInfo { get; set; }

        public DbSet<SiteAllocationInfo> SiteAllocationInfo { get; set; }
        public DbSet<UserAttendanceInfo> UserAttendanceInfo { get; set; }
        public DbSet<ClusterAllocationInfo> ClusterAllocationInfo { get; set; }
        public DbSet<ProjectAllocationInfo> ProjectAllocationInfo { get; set; }
        public DbSet<RegionAllocationInfo> RegionAllocationInfo { get; set; }
        public DbSet<LeaveTypeInfo> LeaveTypeInfo { get; set; }
        public DbSet<LeaveInfo> LeaveInfo { get; set; }

    }
}
