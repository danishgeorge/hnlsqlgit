using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace HNL.EntityFrameworkCore
{
    public static class HNLDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<HNLDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<HNLDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
