﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using HNL.Configuration;
using HNL.Web;

namespace HNL.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class HNLDbContextFactory : IDesignTimeDbContextFactory<HNLDbContext>
    {
        public HNLDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<HNLDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            HNLDbContextConfigurer.Configure(builder, configuration.GetConnectionString(HNLConsts.ConnectionStringName));

            return new HNLDbContext(builder.Options);
        }
    }
}
