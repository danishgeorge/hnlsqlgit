﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HNL.Migrations
{
    public partial class UserAttendance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CheckInFrom",
                table: "UserAttendanceInfo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CheckOutFrom",
                table: "UserAttendanceInfo",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CheckInFrom",
                table: "UserAttendanceInfo");

            migrationBuilder.DropColumn(
                name: "CheckOutFrom",
                table: "UserAttendanceInfo");
        }
    }
}
