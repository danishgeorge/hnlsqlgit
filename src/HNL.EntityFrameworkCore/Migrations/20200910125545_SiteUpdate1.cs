﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HNL.Migrations
{
    public partial class SiteUpdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SiteInfo_VendorInfo_VendorId",
                table: "SiteInfo");

            migrationBuilder.DropTable(
                name: "VendorInfo");

            migrationBuilder.DropIndex(
                name: "IX_SiteInfo_VendorId",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "Category",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "GuardedStatus",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "IntegrationDate",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "Omo",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "OmoId",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "SrNo",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "Vendor",
                table: "SiteInfo");

            migrationBuilder.DropColumn(
                name: "VendorId",
                table: "SiteInfo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Category",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuardedStatus",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IntegrationDate",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Omo",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OmoId",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SrNo",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vendor",
                table: "SiteInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VendorId",
                table: "SiteInfo",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "VendorInfo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorInfo", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SiteInfo_VendorId",
                table: "SiteInfo",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_SiteInfo_VendorInfo_VendorId",
                table: "SiteInfo",
                column: "VendorId",
                principalTable: "VendorInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
