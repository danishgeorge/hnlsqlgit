﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HNL.Migrations
{
    public partial class leavechange1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovedBy",
                table: "leaveInfo");

            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovedDate",
                table: "leaveInfo",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "isCEOApprovalRequired",
                table: "leaveInfo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isCEOApproved",
                table: "leaveInfo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isHODApprovalRequired",
                table: "leaveInfo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isHODApproved",
                table: "leaveInfo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isManagerApprovalRequired",
                table: "leaveInfo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isManagerApproved",
                table: "leaveInfo",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovedDate",
                table: "leaveInfo");

            migrationBuilder.DropColumn(
                name: "isCEOApprovalRequired",
                table: "leaveInfo");

            migrationBuilder.DropColumn(
                name: "isCEOApproved",
                table: "leaveInfo");

            migrationBuilder.DropColumn(
                name: "isHODApprovalRequired",
                table: "leaveInfo");

            migrationBuilder.DropColumn(
                name: "isHODApproved",
                table: "leaveInfo");

            migrationBuilder.DropColumn(
                name: "isManagerApprovalRequired",
                table: "leaveInfo");

            migrationBuilder.DropColumn(
                name: "isManagerApproved",
                table: "leaveInfo");

            migrationBuilder.AddColumn<string>(
                name: "ApprovedBy",
                table: "leaveInfo",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
