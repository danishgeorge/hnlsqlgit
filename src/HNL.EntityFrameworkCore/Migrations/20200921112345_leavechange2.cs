﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HNL.Migrations
{
    public partial class leavechange2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_leaveInfo_LeaveTypeInfo_LeaveTypeId",
                table: "leaveInfo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_leaveInfo",
                table: "leaveInfo");

            migrationBuilder.RenameTable(
                name: "leaveInfo",
                newName: "LeaveInfo");

            migrationBuilder.RenameIndex(
                name: "IX_leaveInfo_LeaveTypeId",
                table: "LeaveInfo",
                newName: "IX_LeaveInfo_LeaveTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LeaveInfo",
                table: "LeaveInfo",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveInfo_LeaveTypeInfo_LeaveTypeId",
                table: "LeaveInfo",
                column: "LeaveTypeId",
                principalTable: "LeaveTypeInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveInfo_LeaveTypeInfo_LeaveTypeId",
                table: "LeaveInfo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LeaveInfo",
                table: "LeaveInfo");

            migrationBuilder.RenameTable(
                name: "LeaveInfo",
                newName: "leaveInfo");

            migrationBuilder.RenameIndex(
                name: "IX_LeaveInfo_LeaveTypeId",
                table: "leaveInfo",
                newName: "IX_leaveInfo_LeaveTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_leaveInfo",
                table: "leaveInfo",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_leaveInfo_LeaveTypeInfo_LeaveTypeId",
                table: "leaveInfo",
                column: "LeaveTypeId",
                principalTable: "LeaveTypeInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
