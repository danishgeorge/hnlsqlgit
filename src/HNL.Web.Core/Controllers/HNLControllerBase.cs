using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace HNL.Controllers
{
    public abstract class HNLControllerBase: AbpController
    {
        protected HNLControllerBase()
        {
            LocalizationSourceName = HNLConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
