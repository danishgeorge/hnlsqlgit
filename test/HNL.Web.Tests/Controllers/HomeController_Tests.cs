﻿using System.Threading.Tasks;
using HNL.Models.TokenAuth;
using HNL.Web.Controllers;
using Shouldly;
using Xunit;

namespace HNL.Web.Tests.Controllers
{
    public class HomeController_Tests: HNLWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}