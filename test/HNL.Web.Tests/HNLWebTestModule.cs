﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using HNL.EntityFrameworkCore;
using HNL.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace HNL.Web.Tests
{
    [DependsOn(
        typeof(HNLWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class HNLWebTestModule : AbpModule
    {
        public HNLWebTestModule(HNLEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(HNLWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(HNLWebMvcModule).Assembly);
        }
    }
}